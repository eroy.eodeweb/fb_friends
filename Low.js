module.exports = function(FB, logsArray) {
    if (!FB) return null;

    // low level api
    var api = function(uri, fields) {
        return new Promise((resolve, reject) => {
            FB.api(uri, (fields ? {fields} : {}), (res) => {
                if (res && !res.error) {
                    resolve(res);
                }
                else {
                    reject(res);
                    logsArray && logsArray.push(res.error);
                }
            });
        });
    };

    // DATA
    var Data = function(obj) {
        obj.prototype.compare = Data.prototype.compare;

        Object.defineProperty(obj.prototype, "_key", {
            get: function() {
                return this.id;
            }
        });
    };
    Data.prototype.compare = function(obj) {
        return this.prototype.isPrototypeOf(obj) && obj.id == this.id;
    };

    var Post = function(id, created_time) {
        this.id = id;
        this.created_time = created_time || "";

        this.since = (Date.now()-(new Date(created_time).getTime()))/86400000; // days
    };
    Data(Post);

    var Like = function(id, name, since) {
        this.id = id;
        this.name = name || "";

        this.since = since || 0;
    };
    Data(Like);

    // api calls
    // get user posts
    this.getPosts = async function() {
        var a = await api("me/posts");
        var res = a.data || [];

        logsArray && logsArray.push("LowApi getPosts: "+res.length+" found");

        var posts = [];
        for (let post of res) {
            if (post.id) {
                posts.push(new Post(post.id, post.created_time));
            }
        }

        return posts;
    };

    // get likes for a post list
    this.getLikes = async function(data, limits) {
        var likes = [];
        
        if (!arguments.length) return likes;

        // case of a single object
        if (!Array.isArray(data)) {
            data = [data];
        }

        // requirements
        limits = limits || {
            require: 0,
            since: -1,
            timeout: -1,
            fbCalls: -1
        };
        var keys = [];
        var requiredKeys = limits.require; // if 0 -> required number reached
        var stopSince = false; // if true, try to stop calls (date)
        var stopCalls = false; // if true, try to stop calls (fb calls)
        var stopTime = false; // if true, try to stop calls (timeout)
        var countRequests = 1;
        var startTime = Date.now();

        // is a direct data list => data[ obj{id: ******}, ... ]
        for (let obj of data) {
            if (obj.id) {
                // date limit
                if (!stopSince && limits.since >= 0 && obj.since && obj.since > limits.since) {
                    logsArray && logsArray.push("Stop[since] "+obj.since+" days");
                    stopSince = true;
                }

                // request
                ++countRequests;
                let a = await api(obj.id+"/likes");
                logsArray && logsArray.push("LowApi getLikes: "+(a.data && a.data.length ? a.data.length : 0)+" found");

                var ls = [];
                for (let like of (a.data || [])) {
                    if (like.id) {
                        ++countRequests;

                        // require, uniqs keys
                        if (keys.indexOf(like.id) < 0) {
                            keys.push(like.id);
                            --requiredKeys;
                        }

                        likes.push(new Like(like.id, like.name, obj.since));

                        // request limit
                        if (!stopCalls && limits.fbCalls >= 0 && countRequests+1 > limits.fbCalls) {
                            stopCalls = true;
                            logsArray && logsArray.push("Stop[calls] "+countRequests+" calls");
                        }

                        // timeout limit
                        if (!stopTime && limits.timeout >= 0 && (limits.timeout < ((Date.now()-startTime)/1000))) {
                            stopTime = true;
                            logsArray && logsArray.push("Stop[timeout] "+((Date.now()-startTime)/1000)+"s");
                        }

                        // try to force stop requests
                        if (requiredKeys <= 0 && (stopSince || stopCalls || stopTime)) {
                            logsArray && logsArray.push("[Stop after "+countRequests+" requests] LowApi getLikes(sum): "+likes.length+" found");
                            return likes;
                        }
                    }
                }
            }
        }

        logsArray && logsArray.push("LowApi getLikes(sum): "+likes.length+" found");

        return likes;
    };

    // gets pictures url of friends
    // data contains: is_silhouette, url
    this.getPictures = async function(userlist, type) {
        var pictureList = {};

        if (!arguments.length) return pictureList;

        //--> multiple lists
        if (arguments.length > 1) {
            let dl = [];
            for (let arg of arguments) {
                dl = dl.concat(arg);
            }

            userlist = dl;
        }

        //--> single
        
        for (let data of userlist) {
            if (data._key) {
                let pic = await api(data._key+"/picture?redirect=0&type="+(type ? type : "square"), ["url", "is_silhouette"]);
                pictureList[data._key] = pic.data;
            }
        }
        
        return pictureList;
    };

    // performe relevance algorithme from like list
    this.relevances = function(datalist) {
        var relevanceList = {};

        if (!arguments.length) return relevanceList;

        //--> multiple lists
        if (arguments.length > 1) {
            let dl = [];
            for (let arg of arguments) {
                dl = dl.concat(arg);
            }

            datalist = dl;
        }

        //--> single
        
        // get all dates values
        let maxValue = -Infinity;
        let minValue = Infinity;
        for (let data of datalist) {
            if (data._key) {
                if (!relevanceList.hasOwnProperty(data._key)) {
                    relevanceList[data._key] = [];
                }

                relevanceList[data._key].push(data.since);

                minValue = Math.min(minValue, data.since);
                maxValue = Math.max(maxValue, data.since);
            }
        }

        // take margin to up min values
        minValue = minValue*0.9;
        maxValue = maxValue-minValue;

        // perform final values
        for (let k in relevanceList) {
            let fv = 0;

            for (let since of relevanceList[k]) {
                fv += ((since-minValue)/maxValue);
            }

            relevanceList[k] = fv;
        }

        return relevanceList;
    };

    // after count, remove duplicates
    this.removeDuplicates = function(datalist) {
        var newData = [];

        if (!arguments.length) return newData;

        //--> multiple lists
        if (arguments.length > 1) {
            let dl = [];
            for (let arg of arguments) {
                dl = dl.concat(arg);
            }

            datalist = dl;
        }

        //--> single
        var keys = [];
        for (let data of datalist) {
            if (data._key) {
                if (keys.indexOf(data._key) < 0) {
                    keys.push(data._key);
                    newData.push(data);
                }
            }
        }

        return newData;
    };
};
