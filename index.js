'use strict'

class API {
    constructor() {
        this.High = new (require("./High"))(this);
        this.Low = require("./Low");
    }
}

module.exports = new API;