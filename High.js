const LowAPI = require("./Low");

// Empty class
class High {
    constructor(api) {
        this.FB = require('fb');

        Object.defineProperty(this, "token", {
            configurable: true,
            set: function(value) {
                api.High = new HighEnabled(api, this.FB, value);
            }
        });

        Object.defineProperty(this, "friends", {
            writable: false,
            value: {}
        });
    }

    reset(){}
    logs() {
        return [
            "Please, set a valid FB token"
        ];
    }

    isReady(){ return false; }
};

// High api Ready
var HighEnabled = function(api, fb, token) {
    // initialize FB module
    fb.setAccessToken(token);

    // publics
    Object.defineProperty(this, "FB", {
        configurable: false,
        get: () => fb
    });

    Object.defineProperty(this, "token", {
        configurable: false,
        get: () => token
    });

    this.isReady = () => true;
    this.logs = function() {
        return this.logs.lines;
    };
    this.logs.lines = [];

    // low api
    var low = new LowAPI(fb, this.logs.lines);
    var require = 0;
    var since = -1;
    var timeout = -1;
    var fbCalls = -1;

    Object.defineProperty(this, "limits", {
        configurable: false,
        get: function() {
            return {
                require,
                since,
                timeout,
                fbCalls
            };
        }
    });

    // friend list of me. First call is async.
    Object.defineProperty(this, "friends", {
        configurable: true,
        get: async function() {
            let pt = Date.now();
            this.logs.lines.push("Gets friends...");

            try {
                // get posts
                let posts = await low.getPosts();
                this.logs.lines.push("Found "+posts.length+" posts...");

                // get likes
                let ptimeout = timeout;
                if (timeout >= 0) {
                    timeout -= ((Date.now()-pt)/1000);
                    timeout /= 2; // prevent picture calls
                }
                let likes = await low.getLikes(posts, this.limits);
                if (timeout >= 0) timeout = ptimeout;
                this.logs.lines.push("Found "+likes.length+" likes...");

                // get pictures
                this.logs.lines.push("Gets pictures...");
                let pics = await low.getPictures(likes);

                // set relevances
                this.logs.lines.push("Create friends cache...");
                let relevances = low.relevances(likes);

                // create cache
                Object.defineProperty(this, "friends", {
                    writable: false,
                    value: new High.prototype.Friendlist(low.removeDuplicates(likes), relevances, pics)
                });

                this.logs.lines.push("Friends gets in "+((Date.now()-pt)/1000)+" seconds.");

                return this.friends;
            }
            catch(err) {
                this.logs.lines.push(err);
            }

            return new High.prototype.Friendlist();
        }
    });

    // min number of friends required
    this.require = function(nb) {
        require = nb;
        return this;
    };

    // limit olds posts (since days)
    this.since = function(days) {
        since = days;
        return this;
    };

    // limit olds posts (since days)
    this.timeout = function(seconds) {
        timeout = seconds;
        return this;
    };

    // limit calls to fb
    this.fbcalls = function(nb) {
        fbCalls = nb;
        return this;
    };

    // reset all stored data
    this.reset = function(tok) {
        let oldLogs = this.logs.lines;
        oldLogs.push("---------------------------------------");
        oldLogs.push("--> Reset module.");

        if (tok) oldLogs.push("User token changed.");

        api.High = new HighEnabled(api, fb, tok || token);
        api.High.logs.lines = oldLogs;
    };
};

High.prototype.Friendlist = function(likes, relevances, pictures) {
    likes = likes || [];
    relevances = relevances || {};
    pictures = pictures || {};

    for (let like of likes) {
        this.push(
            new High.prototype.Friend(
                like,
                relevances.hasOwnProperty(like._key) ? relevances[like._key] : 0.01,
                pictures.hasOwnProperty(like._key) ? pictures[like._key] : null
            )
        );
    }

    // if > 1, up relevance if user has avatar
    this.avatar = function(value) {
        var cl = this.clone();

        for (let friend of cl) {
            friend.relevance *= (!friend.is_silhouette ? value : 1);
        }

        return cl;
    };

    this.close = function(nb) {
        var fl = new High.prototype.Friendlist();

        var data = this.sort((a,b) => a.relevance < b.relevance);
        data = data.slice(0, nb);

        return fl.concat(data);
    };

    this.random = function(nb) {
        var list = this.clone();

        var j,x,i;
        for(i = list.length-1; i > 0; --i) {
            j = Math.floor(Math.random() * (i+1));
            x = list[i];
            list[i] = list[j];
            list[j] = x;
        }

        return nb ? list.slice(0, nb) : list;
    };

};
High.prototype.Friendlist.prototype = Array.prototype;
High.prototype.Friendlist.prototype.clone = function() {
    var data = new High.prototype.Friendlist();
    return data.concat(this);
};
High.prototype.Friendlist.prototype.concat = function(arr) {
    var res = new High.prototype.Friendlist();

    for (let i of this) {
        res.push(i);
    }

    for (let i of arr) {
        res.push(i);
    }

    return res;
};

High.prototype.Friend = function(like, relevance, pic) {
    this.id = like.id;
    this.name = like.name;
    this.picture = pic ? pic.url : "";
    this.is_silhouette = !pic || (pic && pic.is_silhouette);

    this.relevance = relevance || 0.01;
};

module.exports = High;