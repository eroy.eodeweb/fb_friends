const util = require('util');

const myToken = require("./test_token");
var FbFriends = require("./index");

// HLapi initialization
FbFriends.High.FB.options({version: "v2.12"});
FbFriends.High.token = myToken;

if (!FbFriends.High.isReady()) {
    console.log("/!\\ Error, module is not initialized...");
    console.log(util.inspect(FbFriends.High.logs(), false, 4));
    return;
}

// HLapi test
var FbF = FbFriends.High;

console.log("FbFriends initialized.");

// limit requests
FbF.fbcalls(10) // limits to 10 requests
    .timeout(2) // limits request duration in seconds
    .since(500) // since 500 days
    .require(8); // require a minimum of 8 friends

// get friends
console.log("Call FB to get friends...");
(async () => {
    const friends = await FbF.friends;
    const myBests = friends.avatar(2).close(16).random(8);

    if (myBests.length == 8) {
        console.log(util.inspect(myBests, false, 4));
    }
    else {
        console.log("--> ERROR");
    }

    console.log("\n--------------------------------------");
    console.log("--> LOGS");
    console.log(util.inspect(FbF.logs(), false, 4));
})();
