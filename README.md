#Facebook Friends

Get a list of friends from likes.

## FB scope
### Gets Data
- user_posts

### Love Algorithm
- user_birthday [NotImplemented]
- user_relationships [NotImplemented]


## HL api
### Set token
- Use facebook graph tool -> https://developers.facebook.com/tools/explorer
- Generate a token with permissions
- Copy/past the token in test_token.js

### Test cmd
    ```shell
    npm run-script test
    ```

### Api
Initialize the HL api
    ```javascript
    var FbFriends = require("./index");

    // first, set your fb options
    FbFriends.High.FB.options({version: "v2.12"});

    // HL api require a token before start
    FbFriends.High.token = myToken;

    // Gets the actual instance of HL api
    var FbF = FbFriends.High;
    ```

Gets friends *(basic, without limits), return a Friendlist*
    ```javascript
    const friends = await FbF.friends;
    console.log(util.inspect(friends, false, 4));
    ```

Gets logs
    ```javascript
    console.log(util.inspect(FbF.logs(), false, 4));
    ```

Reset cache, *a new token can be assigned*
    ```javascript
    FbF.reset([token]);
    ```

**Limits**
*It is not a filter, all is approximate*

Use require to gets a minimum of **n** friends. Override other limits if the number is not reached.
    ```javascript
    FbF.require(n);
    ```

Use since with nb of days to stop the calls if posts is older than this date.
    ```javascript
    FbF.since(365); // posts of the last year
    ```

Use fbcalls to limits the calls on fb api. This is highly recommanded.
    ```javascript
    FbF.fbcalls(n);
    ```

Use timeout to set a limit in seconds. (its not a failure, if require is validate, just stop calls and return actual list of friends)
    ```javascript
    FbF.timeout(s);
    ```

**Friendlist**
Gets **n** close friends
    ```javascript
    FbF.friends.close(n);
    ```

Gets **n** random friends
    ```javascript
    FbF.friends.random(n);
    ```

Up value of user with avatar *(default is 1. 2 up relevance by x2)*
    ```javascript
    FbF.friends.avatar(x);
    ```

Gets **n2** random of a close friend list[**n1**]
    ```javascript
    FbF.friends.avatar(2).close(n1).random(n2);
    ```